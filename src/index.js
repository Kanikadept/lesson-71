import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from "./App";
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";
import dishesReducer from "./store/reducers/DishesReducer";
import orderReducer from "./store/reducers/OrdersReducer";

const rootReducer = combineReducers({
    dishesRed: dishesReducer,
    ordersRed: orderReducer
})
const store = createStore(rootReducer, applyMiddleware(thunk))

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));