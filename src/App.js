import {Switch, Route} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import Dishes from "./components/Dishes/Dishes";
import './App.css';
import DishEditForm from "./components/DishEditForm/DishEditForm";
import Orders from "./components/Orders/Orders";

const App = () => (
    <div className="App">
      <div className="container">
        <Layout>
          <Switch>
            <Route path="/" exact component={Dishes}/>
              <Route path="/dish-edit/:id" exact component={DishEditForm}/>
              <Route path="/orders" exact component={Orders}/>
          </Switch>
        </Layout>
      </div>
    </div>
);

export default App;
