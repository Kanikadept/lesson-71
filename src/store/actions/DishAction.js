import axios from "axios";

export const requestCreateDish = (dish) => {
    return async dispatch => {
        try {
            await axios.post('https://lesson-71-default-rtdb.firebaseio.com/dishes.json',dish);
            dispatch(requestDishes());
        } catch (e) {

        }
    }
}

export const REQUEST_DISHES_FETCH = 'REQUEST_DISHES_FETCH';
export const requestDishesSuccess = payload => ({type: REQUEST_DISHES_FETCH, payload});
export const requestDishes = () => {
    return async dispatch => {
        try {
            const dishResponse = await axios.get('https://lesson-71-default-rtdb.firebaseio.com/dishes.json');
            dispatch(requestDishesSuccess(dishResponse.data));
        } catch (e) {

        }
    }
}

export const removeDishRequest = (id) => {
    return async dispatch => {
        try {
            await axios.delete(`https://lesson-71-default-rtdb.firebaseio.com/dishes/${id}.json`);
            dispatch(requestDishes());
        } catch (e) {

        }
    }
}

export const handleUpdateForm = (id, obj) => {
    return async dispatch => {
        try {
            await axios.put(`https://lesson-71-default-rtdb.firebaseio.com/dishes/${id}.json`, obj);
            dispatch(requestDishes());
        } catch (e) {

        }
    }
}

export const REQUEST_SINGLE_DISH = 'REQUEST_SINGLE_DISH';
export const requestSingleDishSuccess = (dish) => ({type: REQUEST_SINGLE_DISH, dish})
export const requestSingleDish = (id) => {
    return async dispatch => {
        try {
            const dishResponse = await axios.get(`https://lesson-71-default-rtdb.firebaseio.com/dishes/${id}.json`);
            dispatch(requestSingleDishSuccess(dishResponse.data));
        } catch (e) {

        }
    }
}