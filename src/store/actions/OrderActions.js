import axios from "axios";

export const FETCH_ORDER_REQUEST = 'FETCH_ORDER_REQUEST';
export const fetchOrderSuccess = payload => ({type: FETCH_ORDER_REQUEST, payload})
export const fetchOrder = () => {
    return async (dispatch, getState) => {
        try {
            const ordersResponse = await axios.get('https://lesson-71-default-rtdb.firebaseio.com/orders.json');
            const orders = ordersResponse.data;
            console.log(orders);
            const dishes = getState().dishesRed.dishes;
            console.log(dishes);

            const ordersArr = Object.keys(orders).map(order => {
                return {...orders[order]};
            })
            //=====================
            let orderList = [];
            for (let i = 0; i < ordersArr.length; i++) {

                const order = ordersArr[i];

                const orderItemPromise = Object.keys(order).map(async dish => {
                    const dishResponse = await axios.get(`https://lesson-71-default-rtdb.firebaseio.com/dishes/${dish}.json`);
                    const dishe = dishResponse.data;

                    return {title: dishe.title, price: dishe.price * order[dish]};
                })
                console.log(orderItemPromise);

                const result = Promise.all(orderItemPromise).then(order => {
                    const orderInfo = {dishes: order};
                    const totalPrice = order.reduce((acc, order) => {
                        return acc += order.price;
                    },150)
                    orderInfo.totalPrice = totalPrice;
                    orderInfo.delivery = 150;
                    orderList.push(orderInfo);
                });
            }
            dispatch(fetchOrderSuccess(orderList));
        } catch (e) {

        }
    }
}