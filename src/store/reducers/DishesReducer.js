import {REQUEST_DISHES_FETCH, REQUEST_SINGLE_DISH} from "../actions/DishAction";

const initialState = {
    dishes: {},
    dish: null
}

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case REQUEST_DISHES_FETCH:
            return {...state, dishes: action.payload}
        case REQUEST_SINGLE_DISH:
            return {...state, dish: action.dish}
        default:
            return state;
    }
}

export default dishesReducer;