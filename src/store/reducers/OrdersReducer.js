import {FETCH_ORDER_REQUEST} from "../actions/OrderActions";

const initialState = {
    orders: [],
    dish: null
}

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ORDER_REQUEST:
            return {...state, orders : action.payload};
        default:
            return state;
    }
}

export default orderReducer;