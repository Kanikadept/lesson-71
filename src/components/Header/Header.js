import React from 'react';
import {NavLink} from "react-router-dom";
import {Box, Grid} from "@material-ui/core";

const Header = () => {
    return (
        <Grid style={{marginBottom: '6rem'}}>
            <Box display="flex" justifyContent="space-between">
                <Box>Turtle Pizza Admin</Box>
                <Box>
                    <NavLink style={{marginRight: 11}} to="/">Dishes</NavLink>
                    <NavLink to="/orders">Orders</NavLink>
                </Box>
            </Box>

        </Grid>
    );
};

export default Header;