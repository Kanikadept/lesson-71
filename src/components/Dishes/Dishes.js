import React, {useState} from 'react';
import './Dishes.css';
import Modal from "../UI/Modal/Modal";
import {requestCreateDish} from "../../store/actions/DishAction";
import {useDispatch} from "react-redux";
import DishesList from "./DishesList/DishesList";


const Dishes = (props) => {

    const dispatch = useDispatch();

    const [dish, setDish] = useState({
        title: '',
        price: '',
        image: ''
    });
    const [show, setShow] = useState(false);

    const closeForm = (bool) => {
        setShow(false);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(requestCreateDish(dish));
        closeForm();
        setDish({
            title: '',
            price: '',
            image: ''
        });
    }

    const handleChange = (event) => {
        const dishCopy = {...dish};
        const {name, value} = event.target;
        dishCopy[name] = value;
        setDish(dishCopy);
    }

    return (
        <>
            <Modal show={show} closeForm={closeForm}>
                <form onSubmit={handleSubmit} className="dish-form">
                    <label>title</label>
                    <input value={dish.title} onChange={handleChange} name="title">
                    </input>
                    <label>price</label>
                    <input value={dish.price} onChange={handleChange} name="price" >
                    </input>
                    <label>image</label>
                    <input value={dish.image} onChange={handleChange} name="image">
                    </input>
                    <button type="submit">Create</button>
                </form>
            </Modal>
            <div display="flex">
                <div>Dishes</div>
                <button onClick={() => setShow(true)}>Add new Dish</button>
            </div>
            <DishesList hsitoryTo={props.history}/>
        </>

    );
};

export default Dishes;