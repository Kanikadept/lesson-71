import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {removeDishRequest, requestDishes} from "../../../store/actions/DishAction";
import './DishesList.css';


const DishesList = (props) => {

    const dispatch = useDispatch();
    const dishes = useSelector(state => state.dishesRed.dishes);
    const dish = useSelector(state => state.dishesRed.dish);

    useEffect(() => {
        dispatch(requestDishes());
    }, [dispatch, dish]);

    const handleDelete = (id) => {
        dispatch(removeDishRequest(id));
    }

    const handleUpdate = (id) => {
        props.hsitoryTo.push('/dish-edit/' + id);
    }

    return (
        <div className="dishes-list">
            {dishes ? Object.keys(dishes).map(dish => (
                <div key={dish} className="dish">
                    <div className="image dish-item"><img src={dishes[dish].image} alt=""/></div>
                    <div className="title dish-item"><span>{dishes[dish].title}</span></div>
                    <div className="price dish-item"><span>{dishes[dish].price} KGS</span></div>
                    <div className="btns dish-item">
                        <button onClick={() => handleUpdate(dish)}>Edit</button>
                        <button onClick={() => handleDelete(dish)}>Delete</button>
                    </div>
                </div>
            )) : <div></div>}
        </div>
    );
};

export default DishesList;