import React, {useState, useEffect} from 'react';
import './DishEditForm.css';
import {useDispatch, useSelector} from "react-redux";
import {handleUpdateForm, requestCreateDish, requestSingleDish} from "../../store/actions/DishAction";

const DishEditForm = (props) => {
    const [dish, setDish] = useState({
        title: '',
        price: '',
        image: ''
    });

    const dispatch = useDispatch();
    const dishToUpdate = useSelector(state => state.dishesRed.dish);

    useEffect(() => {
        dispatch(requestSingleDish(props.match.params.id))
    }, [dispatch, props.match.params.id])

    useEffect(() => {
        setDish(dishToUpdate);
    }, [dishToUpdate])



    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(handleUpdateForm(props.match.params.id, dish));
        props.history.push('/');
        setDish({
            title: '',
            price: '',
            image: ''
        });
    }


    const handleChange = (event) => {
        const dishCopy = {...dish};
        const {name, value} = event.target;
        dishCopy[name] = value;
        setDish(dishCopy);
    }


    return ( dish &&
        <form onSubmit={handleSubmit} className="dish-form-edit">
            <label>title</label>
            <input value={dish.title} onChange={handleChange} name="title">
            </input>
            <label>price</label>
            <input value={dish.price} onChange={handleChange} name="price" >
            </input>
            <label>image</label>
            <input value={dish.image} onChange={handleChange} name="image">
            </input>
            <button type="submit">Update</button>
        </form>
    );
};

export default DishEditForm;