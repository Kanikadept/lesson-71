import React, {useEffect} from 'react';
import './Orders.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchOrder} from "../../store/actions/OrderActions";

const Orders = () => {

    const dispatch = useDispatch();
    const orders = useSelector(state => state.ordersRed.orders);

    useEffect(() => {
        dispatch(fetchOrder());
    }, [dispatch])

    return (
        <div className="order-list">
            {/*{orders}*/}
            {console.log(orders, 'here')}
            {orders.map((order, index) => {
                return (

                    <div key={index}>
                        <div>
                            {order.dishes.map(info => {
                                return <div key={info.title}>{info.title} {info.price}</div>
                            })}
                        </div>
                        <div>{order.totalPrice}</div>
                    </div>
                )
            })}
        </div>
    );
};

export default Orders;